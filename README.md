# udpmask

A program that obfuscates outgoing traffic and deobfuscates incomming traffic using libnetfilterqueue.
Does NOT provide any confidentiality or integrity. 

Requires root or CAP_NET_ADMIN and CAP_NET_RAW to function.

Once the program is running, you must redirect the desired traffic to the queue using iptables.

Example: obfuscating traffic to and from 192.168.70.1:7000 with queue number 0
```
iptables -A INPUT -s 192.168.70.1 -p udp --sport 7000 -j NFQUEUE --queue-num 0
iptables -A OUTPUT -d 192.168.70.1 -p udp --dport 7000 -j NFQUEUE --queue-num 0
```
